﻿using System;
using System.Reflection;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Threading;
using SevenZip;

namespace GalleryMover
{
    public partial class Form1 : Form
    {
        GalleryMover galleryMover = new GalleryMover();
        public string sourceDirectory;
        public string targetDirectory;
        public Boolean moveFiles = false;
        private BindingSource source;
        private bool compressingInProgress = false;
        private int totalGalleries;
        private int completedGalleries;
        private float totalProgress;

        public Form1()
        {
            InitializeComponent();
        }

        private void btnSource_Click(object sender, EventArgs e)
        {   
            if(this.folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                this.sourceDirectory = this.folderBrowserDialog1.SelectedPath;
                this.tbSource.Text = this.sourceDirectory;
            }
        }

        private void btnTarget_Click(object sender, EventArgs e)
        {
            if (this.folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                this.targetDirectory = this.folderBrowserDialog1.SelectedPath;
                this.tbTarget.Text = this.targetDirectory;
            }
        }

        private void directories_Change(object sender, EventArgs e)
        {
            if(this.tbSource.Text.Length > 0  && this.tbTarget.Text.Length > 0)
            {
                this.btnStart.Enabled = true;
            }
            else
            {
                this.btnStart.Enabled = false;
            }
        }

        private void startProcess(object sender, EventArgs e)
        {
            backgroundWorker1.DoWork += new DoWorkEventHandler(bw_scan);
            backgroundWorker1.ProgressChanged += new ProgressChangedEventHandler(bw_scanProgresChanged);
            backgroundWorker1.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bw_scanCompleted);

            BindingList<HathGallery> bindinglist = new BindingList<HathGallery>(galleryMover.galleries);
            //BindingSource source = new BindingSource(bindinglist, null);
            source = new BindingSource(bindinglist, null);
            dataGridView1.DataSource = source;

            backgroundWorker1.RunWorkerAsync();
            tslStatus.Text = "Scanning directory ...";
        }

        private void bw_scan(object sender, DoWorkEventArgs e)
        {
            backgroundWorker1 = sender as BackgroundWorker;


            this.targetDirectory = this.tbTarget.Text;
            this.sourceDirectory = this.tbSource.Text;
            galleryMover.targetDirectory = this.targetDirectory;
            galleryMover.setDirectory(this.sourceDirectory);

            List<HathGallery> galleries = galleryMover.getGalleries();
            dataGridView1.DataSource = source;

            galleryMover.e_compressionStarted += new EventHandler<FileNameEventArgs>(sc_compressionStarted);
            galleryMover.e_compressing += new EventHandler<ProgressEventArgs>(sc_compressing);
            galleryMover.e_compressionFinished += new EventHandler<EventArgs>(sc_compressionFinished);

            //Update totalGalleries
            MethodInvoker del = delegate { totalGalleries = galleries.Count; };
            if (InvokeRequired)
                Invoke(del);
            else
                del();

            if (this.cbMove.Checked)
            {
                // Move files to new directory
                // Compress and delete source
                foreach (HathGallery gallery in galleries)
                {
                    this.compressingInProgress = true;
                    //galleryMover.compressCopyGallery(gallery);
                    galleryMover.compressMoveGallery(gallery);
                    while(this.compressingInProgress)
                    {
                        if (compressingInProgress == false)
                            continue;
                    }
                }
            }
            else
            {
                // Copy files to new directory
                // Compress but keep source

                foreach (HathGallery gallery in galleries)
                {
                    this.compressingInProgress = true;
                    galleryMover.compressCopyGallery(gallery);
                    while(this.compressingInProgress)
                    {
                        if (compressingInProgress == false)
                        {
                            tslStatus.Text = DateTime.Now.ToString();
                            continue;
                        }
                    }
                }
            }
        }

        
        private void bw_scanProgresChanged(object sender, ProgressChangedEventArgs e)
        {
            tslStatus.Text = "Scanning ... " + (e.ProgressPercentage.ToString()) + " %";
        }

        private void bw_scanCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled == true)
                tslStatus.Text = "Scan cancelled";

            else if (!(e.Error == null))
                tslStatus.Text = "An error occurred while scanning: " + e.Error.Message;
            else
                tslStatus.Text = "Scan completed";
        }

        private void sc_compressionFinished(object sender, EventArgs e)
        {
            MethodInvoker del = delegate
            {
                this.completedGalleries++;
                float f_progress =  ((float)this.completedGalleries / this.totalGalleries) * 100;
                int progress = (int)f_progress;
                this.totalProgress = f_progress;
                tspTotal.Value = progress;
                tslStatus.Text = f_progress.ToString() + "%  Processed " + this.completedGalleries.ToString() + " / " + this.totalGalleries.ToString() + " galleries";
                compressingInProgress = false;
            };
            if (InvokeRequired)
                Invoke(del);
            else
                del();
            
            compressingInProgress = false;
        }

        private void sc_compressionStarted(object sender, FileNameEventArgs e)
        {
            /*MethodInvoker del = delegate { tslStatus.Text = this.totalProgress.ToString() + "%  Compressing " + e.FileName; };
            if(InvokeRequired)
                Invoke(del);
            else
                del();*/
        }

        private void sc_compressing(object sender, ProgressEventArgs p)
        {
            MethodInvoker del = delegate{ tspCurrent.Value = p.PercentDone; };
            if(InvokeRequired)
            {
                Invoke(del);
            }
            else
            {
                del();
            }
        }
    }
}
