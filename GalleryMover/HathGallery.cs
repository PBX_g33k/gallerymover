﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace GalleryMover
{
    class HathGallery
    {
        public string path { get; set; }
        public string name { get; set; }
        public string galleryInfo { get; set; }
        public bool complete { get; set; }
        public bool processed { get; set; }

        public HathGallery( string path )
        {
            this.path = path;
            this.name = Path.GetFileName(path);
            this.processed = false;
            this.complete = this.getGalleryStatus(path);
        }

        private Boolean getGalleryStatus(string path)
        {
            var file = Directory.GetFiles(path, "galleryinfo.txt", SearchOption.TopDirectoryOnly).FirstOrDefault();
            if (file == null)
            {
                return false;
            }
            else
            {
                this.galleryInfo = File.ReadAllText(file);
                return true;
            }
        }
    }
}
