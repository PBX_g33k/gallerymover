﻿using System;
using System.Collections;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SevenZip;
using System.Reflection;
using System.Configuration;

namespace GalleryMover
{
    class GalleryMover
    {
        public List<HathGallery> galleries = new List<HathGallery>();
        public string sourceDirectory;
        public string targetDirectory;
        private string bit;
        public EventHandler<FileNameEventArgs> e_compressionStarted;
        public EventHandler<EventArgs> e_compressionFinished;
        public EventHandler<ProgressEventArgs> e_compressing;
        

        public GalleryMover()
        {
            if(Environment.Is64BitProcess)
            {
                this.bit = "64";
            }
            else
            {
                this.bit = "32";
            }
            ConfigurationManager.AppSettings["7zLocation"] = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "lib\\"+bit+"\\7za.dll");
            SevenZipExtractor.SetLibraryPath(@ConfigurationManager.AppSettings["7zLocation"]);
        }

        public void setDirectory(string directory)
        {
            this.sourceDirectory = directory;
        }

        public List<HathGallery> getGalleries()
        {
            string[] dirs = getDirectoryContent();

            foreach( string dir in dirs )
            {
                HathGallery gallery = new HathGallery(dir);
                if(gallery.complete)
                    galleries.Add(gallery);
            }

            return galleries;
        }

        private string[] getDirectoryContent()
        {
            string[] directories = Directory.GetDirectories(this.sourceDirectory);
            return directories;
        }

        private void compressGallery(HathGallery gallery)
        {
            compressGallerySevenzip(gallery);
        }

        private void compressGallerySevenzip(HathGallery gallery)
        {
            SevenZipCompressor compressor = new SevenZipCompressor(Path.GetTempPath());
            compressor.ArchiveFormat = OutArchiveFormat.SevenZip;
            compressor.CompressionMode = CompressionMode.Create;
            compressor.CompressionLevel = CompressionLevel.None;
            compressor.CompressionMethod = CompressionMethod.Copy;
            compressor.CompressionFinished += new EventHandler<EventArgs>(e_compressionFinished);
            compressor.Compressing += new EventHandler<ProgressEventArgs>(e_compressing);
            compressor.FileCompressionStarted += new EventHandler<FileNameEventArgs>(e_compressionStarted);
            compressor.BeginCompressDirectory(gallery.path, this.targetDirectory + "\\" + gallery.name + ".7z");
            //compressor.CompressDirectory(gallery.path, this.targetDirectory + "\\" + gallery.name + ".7z");
            gallery.processed = true;
        }

        public void compressMoveGallery(HathGallery gallery)
        {
            this.compressGallery(gallery);
            Directory.Delete(gallery.path,true);
        }

        public void compressCopyGallery(HathGallery gallery)
        {
            this.compressGallery(gallery);
        }
    }
}
